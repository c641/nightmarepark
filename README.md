# NightmarePark

This is a port of the classic PET game "Nightmare Park" to the Commodore 64.

The original was written by Bob Chappel, and released on the 14th May 1980.

This port was converted by Adam Dawes on the 28th December 2019. That's nearly 40 years later! :fearful:

At the moment this is pretty much a straight port of the original game. I tidied up some of the text and controls, fixed a few typos, and got everything working for the C64, but otherwise I've done very little.

In the future I may consider making some small enhancements, to use some colour for example, but I'm reluctant to change too much as I'd prefer to keep the purity and simplicity of the original game.

The code is built using the excellent [C64Studio](http://georg-rottensteiner.de/en/index.html) by Georg Rottensteiner.

If you'd like to contact me, you can do so as follows:

- Email: adam@adamdawes.com
- Twitter: [@AdamDawes575](https://twitter.com/AdamDawes575)
- Web: www.adamdawes.com

Enjoy the game, and don't have nightmares!

